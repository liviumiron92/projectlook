import subprocess
import urllib2
import re
#
# I use xclip to find the last selected text, it should better be a single
# word to allow looking for it in the online dictionary
#
proc = subprocess.Popen(['xclip', '-o'], stdout=subprocess.PIPE, shell=False)
output = proc.communicate()[0]


looked_for = output


#
#   I query the online dictionary for the word and I parse the result
#   to get it's definition
#
url = 'http://dictionary.cambridge.org/dictionary/american-english/'+looked_for


req = urllib2.Request(url, headers={'User-Agent' : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.30 (KHTML, like Gecko) Ubuntu/11.04 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30"})

msg = 'Word not found'
response = None
try:
    response = urllib2.urlopen(req)


    html = response.read()
    found = re.findall('\<meta property\=\"og\:desciption\".+?\>',html, re.DOTALL)




    msg = found[0][found[0].find('content=')+9:found[0].find('See more in')]

    word = msg[:msg.find(' - definition')]
    definition = msg[msg.find(word+': '):]

    msg = definition[:-3]




except urllib2.HTTPError as e:
    code = e.code

    if code == 404:

        msg = 'Word not found'




#
#   I display the results using the notify-send command.
#
not_proc = subprocess.Popen(['notify-send','-t','12000','-u','low', msg], shell=False);

