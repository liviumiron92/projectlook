# README #

This following program will allow for the quick search of a word's definition in an online dictionary. 

### What is this repository for? ###

* Quick summary
       The program will use xclip to get the selected text, urllib2 to get it's definition from the web and notify-send to display the definition as a notification.
* Version 0.6


### How do I get set up? ###

* Dependencies
    * Linux    <- the OS
    * Python(v2)  <- the language used for the program
    * xclip    <- allows the capture of selected text
    * notify-send   <- allows the display of notifications(useful to show the definitions)

    
* Deployment instructions
    Assuming you have Linux(I used Mint but it most likely also work on Ubuntu and other derivatives) you probably also have Python installed. And probably notify-send. All you need to install would be xclip.
    1. Installing the notify-send  

           *sudo apt-get install libnotify-bin*      

    2. Installing xclip

           *sudo apt-get install xclip*

    3. So far everything will be functional but if you want to use it right(in my humble opinion) you must add a keyboard shortcut that will call the script. Thay way looking for the meaning of words when reading something will get really easy. Have fun!
    
    4. If you want limit the number of notifications check out the 
cinnamon_mod directory and read the README

### Who do I talk to? ###

Liviu Miron miron.liviu92@gmail.com
